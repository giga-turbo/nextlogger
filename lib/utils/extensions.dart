// export 'package:flinq/flinq.dart';
// export 'package:styled_widget/styled_widget.dart';
// export 'package:textstyle_extensions/textstyle_extensions.dart';
// export 'package:time/time.dart';

extension RemoveTrailingWhitespaces on String {
  String get nice {
    return replaceAll(RegExp(r'^\s*', multiLine: true), '');
  }
}
