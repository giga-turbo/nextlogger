// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

ThemeData getLightTheme() {
  return ThemeData(
      primarySwatch: Colors.blueGrey,
      accentColor: Colors.blueGrey,
      brightness: Brightness.light); // settings_ui compatibility
}

ThemeData getDarkTheme() {
  final td = ThemeData.dark();
  return ThemeData(
      primarySwatch: Colors.blueGrey,
      scaffoldBackgroundColor: td.scaffoldBackgroundColor,
      canvasColor: td.canvasColor,
      accentColor: Colors.blueGrey, // settings_ui compatibility
      appBarTheme: td.appBarTheme.copyWith(backgroundColor: Colors.blueGrey),
      brightness: Brightness.dark);
}

// ThemeData getBleckTheme() {
//   final td = ThemeData.dark();
//   return ThemeData(
//       primarySwatch: Colors.blueGrey,
//       scaffoldBackgroundColor: Colors.black,
//       canvasColor: Colors.black,
//       accentColor: Colors.blueGrey, // settings_ui compatibility
//       appBarTheme: td.appBarTheme.copyWith(backgroundColor: Colors.blueGrey),
//       brightness: Brightness.dark);
// }
