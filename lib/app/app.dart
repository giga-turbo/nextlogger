import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:stacked_themes/stacked_themes.dart';

import '../services/geolocator_service.dart';
import '../services/settings_service.dart';
import '../services/shared_preferences_service.dart';
import '../viewmodels/settings_view_model.dart';
import '../views/home_view.dart';
import '../views/settings_view.dart';

@StackedApp(routes: [
  MaterialRoute(page: HomeView),
  MaterialRoute(page: SettingsView),
], dependencies: [
  // Services
  Singleton(classType: GeolocatorService),
  Singleton(classType: SharedPreferencesService),
  Singleton(classType: SettingsService),
  // viewmodels
  LazySingleton(classType: SettingsViewModel),
  // stacked services
  Singleton(classType: ThemeService, resolveUsing: ThemeService.getInstance),
  Singleton(classType: NavigationService),
  Singleton(classType: DialogService),
  Singleton(classType: SnackbarService),
  Singleton(classType: BottomSheetService),
], logger: StackedLogger())
class App {}
