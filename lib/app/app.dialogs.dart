import 'package:flutter/material.dart';
import 'package:nextlogger/app/app.locator.dart';
import 'package:nextlogger/datamodels/gps_position.dart';
import 'package:stacked_services/stacked_services.dart';

enum DialogType { details }

void setupDialogUi() {
  final dialogService = locator<DialogService>();

  final builders = {
    DialogType.details: (context, sheetRequest, completer) =>
        _DetailsDialog(request: sheetRequest, completer: completer)
  };

  dialogService.registerCustomDialogBuilders(builders);
}

class _DetailsDialog extends StatelessWidget {
  final DialogRequest request;
  final Function(DialogResponse) completer;
  const _DetailsDialog(
      {Key? key, required this.request, required this.completer})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GPSPosition pos = request.data;
    return Dialog(
        child: Text('Alt: ${pos.altitude}\nLat: ${pos.latitude}\nLon: ${pos.longitude}'));
  }
}
