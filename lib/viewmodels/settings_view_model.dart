import 'package:nextlogger/app/app.logger.dart';
import 'package:nextlogger/services/settings_service.dart';
import 'package:stacked/stacked.dart';

import '../app/app.locator.dart';

class SettingsViewModel extends BaseViewModel {
  final SettingsService _ss = locator<SettingsService>();
  final _logger = getLogger('SettingsViewModel');

  int get logInterval => _ss.logInterval;

  set logInterval(int v) {
    _ss.logInterval = v;
    notifyListeners();
    _logger.d('Updated logInterval to $v');
  }

  int get fixTimeout => _ss.fixTimeout;

  set fixTimeout(int v) {
    _ss.fixTimeout = v;
    notifyListeners();
    _logger.d('Updated fixTimeout to $v');
  }

  int get accuracyTimeout => _ss.accuracyTimeout;

  set accuracyTimeout(int v) {
    _ss.accuracyTimeout = v;
    notifyListeners();
    _logger.d('Updated accuracyTimeout to $v');
  }
}
