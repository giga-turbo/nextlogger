import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:nextlogger/widgets/number_text_form_field.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_hooks/stacked_hooks.dart';
import 'package:stacked_themes/stacked_themes.dart';

import '../app/app.locator.dart';
import '../utils/i18n_extension.dart';
import '../viewmodels/settings_view_model.dart';

class SettingsView extends StatelessWidget {
  SettingsView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // final platformBrightness = MediaQuery.of(context).platformBrightness == Brightness.dark;
    final isDark = Theme.of(context).brightness == Brightness.dark;
    return ViewModelBuilder<SettingsViewModel>.nonReactive(
        viewModelBuilder: () => locator<SettingsViewModel>(),
        disposeViewModel: false,
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
              title: Text('settings.title'.T(context)),
            ),
            body: Form(
              key: _formKey,
              onChanged: () => _formKey.currentState!.validate(),
              onWillPop: () async =>
                  _formKey.currentState!.validate() ? true : false,
              child: SettingsList(
                contentPadding: const EdgeInsets.only(top: 16.0),
                backgroundColor: Colors.transparent,
                sections: [
                  SettingsSection(
                    title: 'settings.general_section'.T(context),
                    tiles: [
                      SettingsTile.switchTile(
                        title: 'settings.darkmode'.T(context),
                        leading: const Icon(Icons.dark_mode),
                        switchValue: isDark,
                        switchActiveColor:
                            Theme.of(context).colorScheme.secondary,
                        onToggle: (bool value) {
                          locator<ThemeService>().setThemeMode(value
                              ? ThemeManagerMode.dark
                              : ThemeManagerMode.light);
                        },
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'settings.gps_section'.T(context),
                    tiles: [
                      SettingsTile(
                        title: 'settings.log_interval'.T(context),
                        subtitle: 'settings.log_interval_desc'.T(context),
                        leading: const Icon(Icons.update),
                        subtitleMaxLines: 4,
                        trailing: _LoggingIntervalForm(),
                      ),
                      SettingsTile(
                        title: 'settings.fix_timeout'.T(context),
                        subtitle: 'settings.fix_timeout_desc'.T(context),
                        leading: const Icon(Icons.hourglass_bottom),
                        subtitleMaxLines: 4,
                        trailing: _FixTimeoutForm(),
                      ),
                      SettingsTile(
                        title: 'settings.after_fix_timeout'.T(context),
                        subtitle: 'settings.after_fix_timeout_desc'.T(context),
                        leading: const Icon(Icons.hourglass_bottom),
                        subtitleMaxLines: 4,
                        trailing: _AfterFixTimeoutForm(),
                      )
                    ],
                  ),
                  SettingsSection(
                    title: 'settings.misc_section'.T(context),
                    tiles: [
                      SettingsTile(
                          title: 'settings.license'.T(context),
                          leading: const Icon(Icons.description),
                          onPressed: (context) {}),
                    ],
                  ),
                ],
              ),
            )));
  }
}

class _LoggingIntervalForm extends HookViewModelWidget<SettingsViewModel> {
  @override
  Widget buildViewModelWidget(
      BuildContext context, SettingsViewModel viewModel) {
    var _loggingInterval =
        useTextEditingController(text: viewModel.logInterval.toString());
    // 0 mean maximum frequency
    // max is 999999 (6 chars)
    return NumberTextFormField(
      controller: _loggingInterval,
      labelText: 'settings.seconds'.T(context),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'settings.enter_value'.T(context);
        } else {
          viewModel.logInterval = int.parse(value);
          return null;
        }
      },
    );
  }
}

class _FixTimeoutForm extends HookViewModelWidget<SettingsViewModel> {
  @override
  Widget buildViewModelWidget(
      BuildContext context, SettingsViewModel viewModel) {
    var _fixTimeout =
        useTextEditingController(text: viewModel.fixTimeout.toString());
    // 0 mean no timeout
    // max 999999 (6)
    return NumberTextFormField(
      controller: _fixTimeout,
      labelText: 'settings.seconds'.T(context),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'settings.enter_value'.T(context);
        } else {
          viewModel.fixTimeout = int.parse(value);
          return null;
        }
      },
    );
  }
}

class _AfterFixTimeoutForm extends HookViewModelWidget<SettingsViewModel> {
  @override
  Widget buildViewModelWidget(
      BuildContext context, SettingsViewModel viewModel) {
    var _timeoutAfterFix =
        useTextEditingController(text: viewModel.accuracyTimeout.toString());
    // 0 mean no timeout
    // max 999999 (6)
    return NumberTextFormField(
      controller: _timeoutAfterFix,
      labelText: 'settings.seconds'.T(context),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'settings.enter_value'.T(context);
        } else {
          viewModel.accuracyTimeout = int.parse(value);
          return null;
        }
      },
    );
  }
}
