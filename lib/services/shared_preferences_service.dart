import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesService {
  SharedPreferences? _prefs;

  Future<SharedPreferences> get prefs async {
    _prefs ??= await SharedPreferences.getInstance();
    return _prefs!;
  }

  Future<String?> getString({required String key}) async {
    return (await prefs).getString(key);
  }

  Future<bool> setString({required String key, required String value}) async {
    return (await prefs).setString(key, value);
  }

  // Future<int?> getInt(String key) async {
  //   return (await prefs).getInt(key);
  // }

  // Future<double?> getDouble(String key) async {
  //   return (await prefs).getDouble(key);
  // }

  // Future<bool?> getBool(String key) async {
  //   return (await prefs).getBool(key);
  // }

  // Future<List<String>?> getStringList(String key) async {
  //   return (await prefs).getStringList(key);
  // }
}
