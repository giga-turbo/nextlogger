import 'dart:async';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';

class BackgroundLocationTaskHandler implements TaskHandler {
  StreamSubscription<Position>? streamSubscription;
  @override
  Future<void> onStart(DateTime timestamp, SendPort? sendPort) async {
    WidgetsFlutterBinding.ensureInitialized();
    final positionStream = Geolocator.getPositionStream();
    streamSubscription = positionStream.listen((position) {
      String date = DateFormat.yMd(Intl.getCurrentLocale())
          .add_Hms()
          .format(position.timestamp!);
      int accuracy = position.accuracy.toInt();

      FlutterForegroundTask.updateService(
          notificationTitle: 'Last log: $date',
          notificationText: 'Accuracy: $accuracy');
      sendPort?.send(position);
    });
  }

  @override
  Future<void> onEvent(DateTime timestamp, SendPort? sendPort) async {}

  @override
  Future<void> onDestroy(DateTime timestamp) async {
    await streamSubscription?.cancel();
  }
}
