import 'package:nextlogger/app/app.locator.dart';
import 'package:nextlogger/app/app.logger.dart';
import 'package:nextlogger/services/shared_preferences_service.dart';
import '../app/app.locator.dart';

enum SelectPositionMethod { last, best }

  
///After [logInterval seconds], wait for a fix for a maximum of [fixTimeout]
///seconds before canceling logging. If fix is obtained, wait for max 
///[fixTimeout] seconds to match the [targetAccuracy]. If it is not
///matched after this timeout the best/last position according to 
///[selectionMethod] is retained. The position is saved if it matchs
///[distanceFilter] and [accuracyFilter].
///
///If no timeouts have been set (0 value) the job ends when the next starts.
///=> Timeouts upper bounds are de facto logInteval
class SettingsService {
  final _sharedPrefsService = locator<SharedPreferencesService>();
  final _loggerS = getLogger('SettingsService');

 
  int _logInterval = 300;
  int _fixTimeout = 120;
  int _accuracyTimeout = 60;
  int _targetAccuracy = 20;
  SelectPositionMethod _selectionMethod = SelectPositionMethod.best;
  int _distanceFilter = 30;
  int _accuracyFilter = 100;

  int get logInterval => _logInterval;
  int get fixTimeout => _fixTimeout;
  int get accuracyTimeout => _accuracyTimeout;
  int get targetAccuracy => _targetAccuracy;
  SelectPositionMethod get selectionMethod => _selectionMethod;
  int get distanceFilter => _distanceFilter;
  int get accuracyFilter => _accuracyFilter;

  set logInterval(int v) {
    _logInterval = v;
    _sharedPrefsService.setString(key: 'logInterval', value: v.toString());
  }

  set fixTimeout(int v) {
    _fixTimeout = v;
    _sharedPrefsService.setString(key: 'fixTimeout', value: v.toString());
  }

  set accuracyTimeout(int v) {
    _accuracyTimeout = v;
    _sharedPrefsService.setString(key: 'accuracyTimeout', value: v.toString());
  }

  set targetAccuracy(int v) {
    _targetAccuracy = v;
    _sharedPrefsService.setString(key: 'targetAccuracy', value: v.toString());
  }

  set selectionMethod(SelectPositionMethod v) {
    _selectionMethod = v;
    _sharedPrefsService.setString(
        key: 'selectionMethod', value: v.index.toString());
  }

  set distanceFilter(int v) {
    _distanceFilter = v;
    _sharedPrefsService.setString(key: 'distanceFilter', value: v.toString());
  }

  set accuracyFilter(int v) {
    _accuracyFilter = v;
    _sharedPrefsService.setString(key: 'accuracyFilter', value: v.toString());
  }

  Future loadSettings() async {
    final prefs = await Future.wait([
      _sharedPrefsService.getString(key: 'logInterval'), //1
      _sharedPrefsService.getString(key: 'fixTimeout'), //2
      _sharedPrefsService.getString(key: 'accuracyTimeout'), //3
      _sharedPrefsService.getString(key: 'targetAccuracy'), //4
      _sharedPrefsService.getString(key: 'selectionMethod'), //5
      _sharedPrefsService.getString(key: 'distanceFilter'), //6
      _sharedPrefsService.getString(key: 'accuracyFilter'), //7
    ]);

    // logInterval
    final li = prefs[0];
    if (li != null) {
      _logInterval = int.parse(li);
    }

    // fixTimeout
    final fi = prefs[1];
    if (fi != null) {
      _fixTimeout = int.parse(fi);
    }

    // accuracyTimeout
    final taf = prefs[2];
    if (taf != null) {
      _accuracyTimeout = int.parse(taf);
    }

    // targetAccuracy
    final ta = prefs[3];
    if (ta != null) {
      _targetAccuracy = int.parse(ta);
    }

    // selectionMethod
    final sm = prefs[4];
    if (sm != null) {
      _selectionMethod = SelectPositionMethod.values[int.parse(sm)];
    }

    // distanceFilter
    final df = prefs[5];
    if (df != null) {
      _distanceFilter = int.parse(df);
    }

    // accuracyFilter
    final af = prefs[6];
    if (af != null) {
      _accuracyFilter = int.parse(af);
    }

    _loggerS.d('Loaded settings');
  }
}
