import 'dart:async';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/loaders/decoders/yaml_decode_strategy.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:nextlogger/datamodels/gps_position.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:stacked_themes/stacked_themes.dart';

import 'app/app.locator.dart';
import 'app/app.router.dart';
import 'app/app.dialogs.dart';
import 'app/app.themes.dart';
import 'services/geolocator_service.dart';
import 'views/home_view.dart';
import 'services/settings_service.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
    translationLoader: FileTranslationLoader(
        useCountryCode: false, fallbackFile: 'en', basePath: 'assets/locales',
        // forcedLocale: const Locale('fr', ''),
        decodeStrategies: [YamlDecodeStrategy()]),
  );
  setupLocator(); // maybe await
  setupDialogUi();
  await ThemeManager.initialise();
  runApp(NextLoggerApp(flutterI18nDelegate));
}

void startCallback() {
  FlutterForegroundTask.setTaskHandler(FirstTaskHandler());
}

class NextLoggerApp extends StatefulWidget {
  final FlutterI18nDelegate flutterI18nDelegate;

  const NextLoggerApp(this.flutterI18nDelegate, {Key? key}) : super(key: key);

  @override
  State<NextLoggerApp> createState() => _NextLoggerAppState();
}

class _NextLoggerAppState extends State<NextLoggerApp> {
  ReceivePort? _receivePort;

  Future _initForegroundTask() async {
    await FlutterForegroundTask.init(
      androidNotificationOptions: const AndroidNotificationOptions(
        channelId: 'notification_channel_id',
        channelName: 'Foreground Notification',
        channelDescription:
            'This notification appears when the foreground service is running.',
        channelImportance: NotificationChannelImportance.LOW,
        priority: NotificationPriority.LOW,
        iconData: NotificationIconData(
          resType: ResourceType.mipmap,
          resPrefix: ResourcePrefix.ic,
          name: 'launcher',
        ),
      ),
      iosNotificationOptions: const IOSNotificationOptions(
        showNotification: true,
        playSound: false,
      ),
      foregroundTaskOptions: const ForegroundTaskOptions(
        interval: 1000,
        autoRunOnBoot: true,
      ),
      printDevLog: true,
    );
  }

  Future _startForegroundTask() async {
    await FlutterForegroundTask.saveData('customData', 'hello');

    _receivePort = await FlutterForegroundTask.startService(
      notificationTitle: 'Foreground Service is running',
      notificationText: 'Tap to return to the app',
      callback: startCallback,
    );
  }

  Future _stopForegroundTask() async {
    await FlutterForegroundTask.stopService();
  }

  @override
  void initState() {
    super.initState();
    _initForegroundTask();
  }

  @override
  void dispose() {
    _receivePort?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ThemeBuilder(
        defaultThemeMode: ThemeMode.system,
        darkTheme: getDarkTheme(),
        lightTheme: getLightTheme(),
        builder: (context, regularTheme, darkTheme, themeMode) => MaterialApp(
              title: 'nextlogger',
              theme: regularTheme,
              darkTheme: darkTheme,
              themeMode: themeMode,
              navigatorKey: StackedService.navigatorKey,
              onGenerateRoute: StackedRouter().onGenerateRoute,
              localizationsDelegates: [
                widget.flutterI18nDelegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              builder: FlutterI18n.rootAppBuilder(),
              home: WithForegroundTask(
                child: FutureBuilder(
                  future: Future.wait([
                    locator<SettingsService>().loadSettings(),
                    locator<GeolocatorService>().request()
                  ]),
                  builder: (context, snapshot) =>
                      snapshot.connectionState == ConnectionState.done
                          ? Scaffold(
                              appBar: AppBar(),
                              body: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ElevatedButton(
                                        onPressed: () async =>
                                            await _initForegroundTask(),
                                        child: const Text('Init')),
                                    ElevatedButton(
                                        onPressed: () async =>
                                            await _startForegroundTask(),
                                        child: const Text('Start')),
                                    ElevatedButton(
                                        onPressed: () async =>
                                            await _stopForegroundTask(),
                                        child: const Text('Stop'))
                                  ],
                                ),
                              ),
                            )
                          : const Scaffold(
                              backgroundColor: Colors.black,
                              body: Center(child: CircularProgressIndicator())),
                ),
              ),
            ));
  }
}

class FirstTaskHandler implements TaskHandler {
  StreamSubscription<Position>? sc;

  @override
  Future<void> onStart(DateTime timestamp, SendPort? sendPort) async {
    WidgetsFlutterBinding.ensureInitialized();
  }

  @override
  Future<void> onEvent(DateTime timestamp, SendPort? sendPort) async {
    sc ??= Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.best,
            intervalDuration: const Duration(seconds: 1))
        .listen((pos) {
      final String date = DateFormat.yMd(Intl.getCurrentLocale())
          .add_Hms()
          .format(pos.timestamp!);

      FlutterForegroundTask.updateService(
          notificationTitle: 'Log at: $date',
          notificationText: '${pos.altitude.toInt()}');
    });
  }

  @override
  Future<void> onDestroy(DateTime timestamp) async {
    sc!.cancel();
  }
}
