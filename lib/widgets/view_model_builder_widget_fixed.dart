import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';

abstract class ViewModelBuilderWidget<T extends ChangeNotifier>
    extends StatelessWidget {
  const ViewModelBuilderWidget({Key? key}) : super(key: key);

  Widget builder(
    BuildContext context,
    T viewModel,
    Widget? child,
  );

  T viewModelBuilder(BuildContext context);

  bool get reactive => true;

  bool get createNewModelOnInsert => false;

  bool get disposeViewModel => true;

  bool get initialiseSpecialViewModelsOnce => false;

  bool get fireOnModelReadyOnce => false;

  void onViewModelReady(T viewModel) {}

  Widget? staticChildBuilder(BuildContext context) => null;

  @override
  Widget build(BuildContext context) {
    if (reactive) {
      return ViewModelBuilder<T>.reactive(
          builder: builder,
          viewModelBuilder: () => viewModelBuilder(context),
          staticChild: staticChildBuilder(context),
          onModelReady: onViewModelReady,
          disposeViewModel: disposeViewModel,
          createNewModelOnInsert: createNewModelOnInsert,
          initialiseSpecialViewModelsOnce: initialiseSpecialViewModelsOnce,
          fireOnModelReadyOnce: fireOnModelReadyOnce);
    } else {
      return ViewModelBuilder<T>.nonReactive(
          builder: builder,
          viewModelBuilder: () => viewModelBuilder(context),
          onModelReady: onViewModelReady,
          disposeViewModel: disposeViewModel,
          createNewModelOnInsert: createNewModelOnInsert,
          initialiseSpecialViewModelsOnce: initialiseSpecialViewModelsOnce,
          fireOnModelReadyOnce: fireOnModelReadyOnce);
    }
  }
}
