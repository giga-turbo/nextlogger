import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nextlogger/datamodels/gps_position.dart';

class PositionTile extends StatelessWidget {
  final GPSPosition position;

  const PositionTile(
    this.position, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.gps_fixed, color: Colors.blue),
      title: Text(
          "${DateFormat.yMd(Intl.getCurrentLocale()).format(position.date)} ${DateFormat.Hms(Intl.getCurrentLocale()).format(position.date)}"),
      subtitle: Text(
          "lon:${position.longitude} lat:${position.latitude} acc:${position.accuracy}"),
    );
  }
}
