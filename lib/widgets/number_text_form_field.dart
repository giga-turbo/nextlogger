import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NumberTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final FormFieldValidator<String>? validator;
  final String labelText;
  final int maxLength;

  const NumberTextFormField(
      {Key? key,
      required this.controller,
      required this.validator,
      required this.labelText,
      this.maxLength = 6})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 100,
        height: 45,
        child: TextFormField(
            controller: controller,
            maxLength: maxLength,
            maxLines: 1,
            autocorrect: false,
            validator: validator,
            onFieldSubmitted: (value) {
              final int? parsed = int.tryParse(value);
              if (parsed != null) controller.text = parsed.toString();
            },
            onTap: () => controller.selection = TextSelection(
                baseOffset: 0, extentOffset: controller.value.text.length),
            decoration: InputDecoration(
                labelText: labelText,
                counterText: "",
                border: const OutlineInputBorder()),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ]));
  }
}
