import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

import '../app/app.router.dart';
import '../app/app.locator.dart';
import '../utils/i18n_extension.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'drawer.title'.T(context),
              style: const TextStyle(fontSize: 28),
            )),
        ListTile(
          title: Text('settings.title'.T(context)),
          leading: const Icon(Icons.settings),
          onTap: () {
            Navigator.pop(context);
            locator<NavigationService>().navigateTo(Routes.settingsView);
          },
        ),
      ],
    ));
  }
}
