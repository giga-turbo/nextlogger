import 'package:geolocator/geolocator.dart';
import 'package:json_annotation/json_annotation.dart';

part 'gps_position.g.dart';

@JsonSerializable()
class GPSPosition {
  final double latitude;
  final double longitude;
  final double altitude;
  final double accuracy;
  final double speed;
  final DateTime date;
  final int battery;

  GPSPosition(
      {required this.latitude,
      required this.longitude,
      required this.altitude,
      required this.accuracy,
      required this.speed,
      required this.date,
      required this.battery});

  factory GPSPosition.fromJson(Map<String, dynamic> json) =>
      _$GPSPositionFromJson(json);

  factory GPSPosition.fromPosition(Position p) => GPSPosition(
      latitude: p.latitude,
      longitude: p.longitude,
      altitude: p.altitude,
      speed: p.speed,
      accuracy: p.accuracy,
      date: p.timestamp!,
      battery: 55); //TODO

  factory GPSPosition.none() => GPSPosition(
      latitude: -1,
      longitude: -1,
      altitude: -1,
      speed: -1,
      accuracy: -1,
      date: DateTime(-1),
      battery: -1);

  Map<String, dynamic> toJson() => _$GPSPositionToJson(this);
}
