# nextlogger

A simple Nextcloud GPS logger

# Building apps

## Download dependencies and generate code

```bash
flutter channel stable
plutter upgrade
```

```bash
flutter clean
flutter pub get
flutter packages pub run build_runner build --delete-conflicting-outputs
flutter pub run flutter_launcher_icons:main
```

## Android app

Build apk:
```bash
flutter build apk --release --obfuscate --split-debug-info=out/android-apk --no-pub
```

Install apk:
```bash
flutter install
```

## Web app

Build app:
```bash
flutter build web --release --web-renderer canvaskit --no-pub
```

Run a web server:
```bash
python -m http.server 8000
```

Navigate to `localhost:8000` in your web browser.